package br.fcv.web;

import static java.util.Objects.requireNonNull;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

import br.fcv.core.TimeZone;
import br.fcv.core.User;

public class TimeZoneWithFormattedTime {

    private final TimeZone timeZone;
    private final LocalTimeInformation localTime;

    public TimeZoneWithFormattedTime(TimeZone timeZone) {
        this(timeZone, null, null);
    }

    public TimeZoneWithFormattedTime(TimeZone timeZone, Instant instant) {
        this(timeZone, instant, null);
    }

    public TimeZoneWithFormattedTime(TimeZone timeZone, Locale locale) {
        this(timeZone, null, locale);
    }

    public TimeZoneWithFormattedTime(TimeZone timeZone, Instant instant, Locale locale) {
        this.timeZone = requireNonNull(timeZone, "timeZone cannot be null");
        if (instant == null) {
            instant = Instant.now();
        }
        this.localTime = new LocalTimeInformation(timeZone, instant, locale);
    }

    public Long getId() {
        return timeZone.getId();
    }

    public String getName() {
        return timeZone.getName();
    }

    public String getCity() {
        return timeZone.getCity();
    }

    public int getMinutesOffset() {
        return timeZone.getMinutesOffset();
    }

    public User getCreatedBy() {
        return timeZone.getCreatedBy();
    }

    public LocalTimeInformation getLocalTime() {
        return localTime;
    }

    public static class LocalTimeInformation {

        private final TimeZone zone;
        private final Instant instant;
        private final Locale locale;

        public LocalTimeInformation(TimeZone zone, Instant instant, Locale locale) {
            this.zone = requireNonNull(zone, "zone cannot be null");
            this.instant = requireNonNull(instant, "instant cannot be null");
            this.locale = locale;
        }

        public Instant getInstant() {
            return instant;
        }

        public String getIsoFormattedTime() {
            return DateTimeFormatter.ISO_INSTANT.format(instant);
        }

        public String getFormattedTime() {
            DateTimeFormatter defaultFormatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.LONG);
            return getFormattedTime(this.instant, defaultFormatter);
        }

        public String getFormattedTime(Instant instant, DateTimeFormatter formatter) {

            Locale locale = this.locale;
            if (locale == null) {
                locale = Locale.getDefault();
            }

            int minutesOffset = zone.getMinutesOffset();
            int hours = minutesOffset / 60;
            int minutesOfHour = minutesOffset % 60;
            ZoneOffset zoneOffset = ZoneOffset.ofHoursMinutes(hours, minutesOfHour);
            String formattedValue = formatter.withZone(zoneOffset).withLocale(locale).format(instant);
            return formattedValue;
        }
    }
}
