package br.fcv.web;

import static org.slf4j.LoggerFactory.getLogger;
import static org.springframework.http.HttpStatus.NOT_FOUND;

import javax.persistence.EntityNotFoundException;

import org.slf4j.Logger;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice(basePackageClasses = RestControllerAdvice.class)
public class RestControllerAdvice {

    private static final Logger logger = getLogger(RestControllerAdvice.class);

    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseStatus(code = NOT_FOUND)
    public void handleEntityNotFoundException(EntityNotFoundException ex) {
        logger.debug("handleEntityNotFoundException()", ex);
    }

}
