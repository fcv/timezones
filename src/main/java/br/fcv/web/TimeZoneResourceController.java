package br.fcv.web;

import static java.util.stream.Collectors.toList;
import static org.slf4j.LoggerFactory.getLogger;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

import java.time.Instant;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import org.slf4j.Logger;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.fcv.core.TimeZone;
import br.fcv.core.TimeZoneService;
import br.fcv.core.User;
import br.fcv.web.support.auth.LoggedUser;

@RestController
@RequestMapping("/api/rest/v1/timezones")
public class TimeZoneResourceController {

    private static final Logger logger = getLogger(TimeZoneResourceController.class);

    private final TimeZoneService service;

    public TimeZoneResourceController(TimeZoneService service) {
        this.service = Objects
                .requireNonNull(service, "service cannot be null");
    }

    @RequestMapping(method = GET)
    public List<TimeZoneWithFormattedTime> list(@RequestParam(required = false, defaultValue = "") String nameFragment,
            @LoggedUser User currentUser,
            Locale locale) {

        logger.debug("list(nameFragment: {}, currentUser: {}, locale: {})", nameFragment, currentUser, locale);
        List<TimeZone> zones;
        if (StringUtils.hasText(nameFragment)) {
            zones = service.listByNameFragment(nameFragment, currentUser);
        } else {
            zones = service.list(currentUser);
        }
        Instant now = Instant.now();
        return zones.stream()
                .map(zone -> new TimeZoneWithFormattedTime(zone, now, locale))
                .collect(toList());
    }

    @RequestMapping(method = POST)
    @ResponseStatus(CREATED)
    public TimeZone create(@RequestBody TimeZone timeZone, @LoggedUser User user) {
        logger.debug("create(timeZone: {}, user: {})", timeZone, user);
        timeZone.setCreatedBy(user);
        return service.create(timeZone);
    }

    @RequestMapping(value = "/{id}", method = GET)
    public TimeZoneWithFormattedTime get(@PathVariable Long id,
            @LoggedUser User currentUser,
            Locale locale) {

        logger.debug("get(id: {}, currentUser: {})", id, currentUser);
        TimeZone zone = service.get(id, currentUser);
        return new TimeZoneWithFormattedTime(zone, locale);
    }

    @RequestMapping(value = "/{id}", method = PUT)
    public TimeZone update(@PathVariable Long id, @RequestBody TimeZone timeZone, @LoggedUser User currentUser) {
        logger.debug("update(id: {}, timeZone: {})", id, timeZone);
        TimeZone zone = service.get(id, currentUser);
        zone.setCity(timeZone.getCity());
        zone.setName(timeZone.getName());
        zone.setMinutesOffset(timeZone.getMinutesOffset());
        return service.update(zone, currentUser);
    }

    @RequestMapping(value = "/{id}", method = DELETE)
    @ResponseStatus(NO_CONTENT)
    public void remove(@PathVariable Long id, @LoggedUser User currentUser) {
        logger.debug("remove(id: {}, currentUser: {})", id, currentUser);
        service.remove(id, currentUser);
    }

}
