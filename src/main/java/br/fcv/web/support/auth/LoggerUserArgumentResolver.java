package br.fcv.web.support.auth;

import static java.util.Objects.requireNonNull;

import javax.inject.Inject;

import org.springframework.core.MethodParameter;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import br.fcv.core.User;
import br.fcv.core.UserService;

public class LoggerUserArgumentResolver implements HandlerMethodArgumentResolver {

    private UserService userService;

    @Inject
    public LoggerUserArgumentResolver(UserService userService) {
        this.userService = requireNonNull(userService, "userService cannot be null");
    }

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        boolean supportsParameter = isUserType(parameter) && isMakedAsLoggedUser(parameter);
        return supportsParameter;
    }

    private boolean isMakedAsLoggedUser(MethodParameter parameter) {
        boolean isMarkedAsLoggedUser = parameter.getParameterAnnotation(LoggedUser.class) != null;
        return isMarkedAsLoggedUser;
    }

    private boolean isUserType(MethodParameter parameter) {
        boolean isUserType = parameter.getParameterType().isAssignableFrom(User.class);
        return isUserType;
    }

    @Override
    public Object resolveArgument(MethodParameter parameter,
            ModelAndViewContainer mavContainer,
            NativeWebRequest webRequest, WebDataBinderFactory binderFactory)
            throws Exception {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null || !authentication.isAuthenticated() ||
                (authentication instanceof AnonymousAuthenticationToken)) {

            throw new LoggedUserNotFoundException();
        }

        String username = authentication.getName();
        User user = userService.findByUsername(username);
        if (user == null) {
            throw new LoggedUserNotFoundException("no User found for username " + username);
        }
        return user;
    }

}
