package br.fcv.web.support.auth;

public class LoggedUserNotFoundException extends RuntimeException {

    private static final long serialVersionUID = -7357610079255899963L;

    public LoggedUserNotFoundException() {
    }

    public LoggedUserNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public LoggedUserNotFoundException(String message) {
        super(message);
    }

    public LoggedUserNotFoundException(Throwable cause) {
        super(cause);
    }

}
