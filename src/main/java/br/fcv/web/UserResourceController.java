package br.fcv.web;

import static java.util.Objects.requireNonNull;
import static org.slf4j.LoggerFactory.getLogger;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import org.slf4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.fcv.core.Role;
import br.fcv.core.User;
import br.fcv.core.UserService;

@RestController
@RequestMapping("/api/rest/v1/users")
public class UserResourceController {

    private static final Logger logger = getLogger(UserResourceController.class);

    private final UserService service;

    public UserResourceController(UserService service) {
        this.service = requireNonNull(service, "service cannot be null");
    }

    @RequestMapping(method = GET)
    @Secured({Role.Name.SYS_ADMIN, Role.Name.MANAGER})
    public Iterable<User> list() {
        logger.debug("list()");
        Iterable<User> users = service.listAll();
        return users;
    }

    @RequestMapping(method = POST)
    @ResponseStatus(CREATED)
    @Secured({Role.Name.SYS_ADMIN})
    public User create(@RequestBody User user) {
        logger.debug("create(User: {})", user);
        return service.create(user.getUsername(), user.getPassword(), user.getRole());
    }

    @RequestMapping(value = "/{id}", method = GET)
    @Secured({Role.Name.SYS_ADMIN, Role.Name.MANAGER})
    public User get(@PathVariable Long id) {
        logger.debug("get(id: {})", id);
        User user = service.get(id);
        return user;
    }

    @RequestMapping(value = "/{id}", method = DELETE)
    @ResponseStatus(NO_CONTENT)
    @Secured({Role.Name.SYS_ADMIN})
    public void remove(@PathVariable Long id) {
        logger.debug("remove(id: {})", id);
        service.remove(id);
    }
}
