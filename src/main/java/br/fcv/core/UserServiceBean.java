package br.fcv.core;

import static java.lang.String.format;
import static java.util.Objects.requireNonNull;
import static org.slf4j.LoggerFactory.getLogger;

import java.util.Collection;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserServiceBean implements UserService {

    private static final Logger logger = getLogger(UserServiceBean.class);

    private final UserRepository repository;
    private final PasswordEncoder passwordEncoder;

    @Inject
    public UserServiceBean(UserRepository repository, PasswordEncoder passwordEncoder) {
        this.repository = requireNonNull(repository, "repository cannot be null");
        this.passwordEncoder = requireNonNull(passwordEncoder, "passwordEncoder cannot be null");
    }

    @Override
    public User findByUsername(String username) {
        logger.debug("findByUsername(username: {})", username);
        User user = repository.findByUsername(username);
        return user;
    }

    @Override
    public User create(String username, String rawPassword, Role role) {
        logger.debug("create(username: {}, rawPassword: <<omitted>>, role: {})", username, role);
        if (repository.countByUsername(username) > 0) {
            throw new UsernameAlreadyTakenException(format("Username '%s' has already been taken", username));
        }
        String password = passwordEncoder.encode(rawPassword);
        User user = new User(username, password, role);
        user = repository.save(user);
        return user;
    }

    @Override
    public void remove(Long id) {
        logger.debug("remove(id: {})", id);
        User user = repository.findExpected(id);
        repository.delete(user);
    }

    @Override
    public User get(Long id) {
        logger.debug("get(id: {})", id);
        User user = repository.findExpected(id);
        return user;
    }

    @Override
    public Iterable<User> listAll() {
        logger.debug("listAll()");
        return repository.findAll();
    }

    @Override
    public Iterable<User> listByRoleIn(Collection<Role> roles) {
        logger.debug("listByRoleIn(roles: {})", roles);
        return repository.findByRoleIn(roles);
    }
}
