package br.fcv.core;

import static br.fcv.core.Role.USER;
import static com.google.common.collect.Lists.newArrayList;
import static java.util.Arrays.asList;
import static org.slf4j.LoggerFactory.getLogger;

import java.util.List;
import java.util.Objects;

import javax.persistence.EntityNotFoundException;

import org.slf4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class TimeZoneServiceBean implements TimeZoneService {

    private static final Logger logger = getLogger(TimeZoneServiceBean.class);

    private final TimeZoneRepository repository;

    public TimeZoneServiceBean(TimeZoneRepository repository) {
        this.repository = Objects.requireNonNull(repository,
                "repository cannot be null");
    }

    @Override
    public TimeZone create(TimeZone zone) {
        logger.debug("create(zone: {})", zone);
        return repository.save(zone);
    }

    @Override
    public TimeZone get(Long id, User currentUser) {
        logger.debug("get(id: {}, currentUser: {})", id, currentUser);
        TimeZone zone = repository.findExpected(id);
        if (!canView(zone, currentUser)) {
            throw new EntityNotFoundException();
        }
        return zone;
    }

    @Override
    public TimeZone remove(Long id, User currentUser) {
        logger.debug("remove(id: {}, currentUser: {})", id, currentUser);
        TimeZone zone = repository.findExpected(id);
        if (!canRemove(zone, currentUser)) {
            throw new EntityNotFoundException();
        }
        repository.delete(zone);
        return zone;
    }

    @Override
    public TimeZone update(TimeZone zone, User currentUser) {
        logger.debug("update(zone: {}, currentUser: {})", zone, currentUser);
        if (!canUpdate(zone, currentUser)) {
            throw new EntityNotFoundException();
        }
        return repository.save(zone);
    }

    @Override
    public List<TimeZone> listByNameFragment(String nameFragment, User currentUser) {
        logger.debug("listByNameFragment(nameFragment: {}, currentUser: {})", nameFragment, currentUser);

        Iterable<TimeZone> zones;
        Role currentUserRole = currentUser.getRole();
        switch (currentUserRole) {
        case USER:
            zones = repository.findByNameContainingIgnoreCaseAndCreatedBy(nameFragment,currentUser);
            break;
        case MANAGER:
            zones = repository.findByNameContainingIgnoreCaseAndCreatedByRoleIn(nameFragment, asList(USER));
            break;
        case SYS_ADMIN:
            zones = repository.findByNameContainingIgnoreCase(nameFragment);
            break;
        default:
            throw new IllegalStateException("Unexpected Role value " + currentUserRole);
        }
        return newArrayList(zones);
    }

    @Override
    public List<TimeZone> list(User currentUser) {
        logger.debug("list(currentUser: {})", currentUser);

        Iterable<TimeZone> zones;
        Role currentUserRole = currentUser.getRole();
        switch (currentUserRole) {
        case USER:
            zones = repository.findByCreatedBy(currentUser);
            break;
        case MANAGER:
            zones = repository.findByCreatedByRoleIn(asList(USER));
            break;
        case SYS_ADMIN:
            zones = repository.findAll();
            break;
        default:
            throw new IllegalStateException("Unexpected Role value " + currentUserRole);
        }
        return newArrayList(zones);
    }

    public boolean canView(TimeZone zone, User currentUser) {
        boolean canView = false;
        Role currentUserRole = currentUser.getRole();
        switch (currentUserRole) {
        case USER:
            canView = zone.getCreatedBy().getId().equals(currentUser.getId());
            break;
        case MANAGER:
            canView = Role.USER.equals(zone.getCreatedBy().getRole());
            break;
        case SYS_ADMIN:
            canView = true;
            break;
        default:
            throw new IllegalStateException("Unexpected Role value " + currentUserRole);
        }
        return canView;
    }

    public boolean canRemove(TimeZone zone, User currentUser) {
        return canView(zone, currentUser);
    }

    public boolean canUpdate(TimeZone zone, User currentUser) {
        return canView(zone, currentUser);
    }
}
