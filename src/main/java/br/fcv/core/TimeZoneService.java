package br.fcv.core;

import java.util.List;

public interface TimeZoneService {

    public TimeZone create(TimeZone zone);

    public TimeZone get(Long id, User currentUser);

    public TimeZone remove(Long id, User currentUser);

    public TimeZone update(TimeZone zone, User currentUser);

    public List<TimeZone> list(User currentUser);

    public List<TimeZone> listByNameFragment(String nameFragment, User currentUser);

}
