package br.fcv.core.support.spring_data;

import java.io.Serializable;

import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface FindExpectedRepositoryCustom<Entity, Id extends Serializable> {

    public Entity findExpected(Id id);

}
