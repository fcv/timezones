package br.fcv.core;

public enum Role {

    USER(Role.Name.USER),
    MANAGER(Role.Name.MANAGER),
    SYS_ADMIN(Role.Name.SYS_ADMIN);

    /**
     * "ROLE_" prefixed version of role's name
     * Useful for Spring Security since Spring Security expects role authorities
     * to be prefixed with "ROLE_".
     */
    private final String roleName;

    private Role(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleName() {
        return roleName;
    }

    public static class Name {

        public static final String USER = "ROLE_USER";
        public static final String MANAGER = "ROLE_MANAGER";
        public static final String SYS_ADMIN = "ROLE_SYS_ADMIN";

    }
}
