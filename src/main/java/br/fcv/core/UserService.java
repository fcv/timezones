package br.fcv.core;

import java.util.Collection;

public interface UserService {

    public User findByUsername(String username);

    public User create(String username, String rawPassword, Role role);

    public void remove(Long id);

    public User get(Long id);

    public Iterable<User> listAll();

    public Iterable<User> listByRoleIn(Collection<Role> roles);

}
