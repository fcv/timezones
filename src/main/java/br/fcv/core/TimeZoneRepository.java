package br.fcv.core;

import java.util.Collection;

import org.springframework.data.repository.CrudRepository;

import br.fcv.core.support.spring_data.FindExpectedRepositoryCustom;

public interface TimeZoneRepository extends FindExpectedRepositoryCustom<TimeZone, Long>, CrudRepository<TimeZone, Long> {

    public Iterable<TimeZone> findByNameContainingIgnoreCase(String nameFragment);

    public Iterable<TimeZone> findByCreatedByRoleIn(Collection<Role> roles);

    public Iterable<TimeZone> findByNameContainingIgnoreCaseAndCreatedByRoleIn(String nameFragment, Collection<Role> roles);

    public Iterable<TimeZone> findByCreatedBy(User createdBy);

    public Iterable<TimeZone> findByNameContainingIgnoreCaseAndCreatedBy(String nameFragment, User createdBy);
}
