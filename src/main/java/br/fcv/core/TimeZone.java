package br.fcv.core;

import static java.lang.String.format;
import static javax.persistence.FetchType.LAZY;
import static javax.persistence.GenerationType.AUTO;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Entity
@Table(name = "timezone")
public class TimeZone {

    @Id
    @GeneratedValue(strategy = AUTO)
    private Long id;
    private String name;
    private String city;

    @Max(720)
    @Min(-720)
    @Column(name = "minutes_offset")
    private int minutesOffset;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "created_by")
    private User createdBy;

    public TimeZone() {
        this(null, null, null);
    }

    public TimeZone(String name, String city) {
        this(null, name, city);
    }

    public TimeZone(String name, String city, int minutesOffset) {
        this(null, name, city, minutesOffset);
    }

    public TimeZone(Long id, String name, String city) {
        this.id = id;
        this.name = name;
        this.city = city;
    }

    public TimeZone(Long id, String name, String city, int minutesOffset) {
        this.id = id;
        this.name = name;
        this.city = city;
        this.minutesOffset = minutesOffset;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getMinutesOffset() {
        return minutesOffset;
    }

    public void setMinutesOffset(int minutesOffset) {
        this.minutesOffset = minutesOffset;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    public String toString() {
        return format("{id: %s, name: %, city: %, minutesOffset: %s}", getId(),
                getName(),
                getCity(),
                getMinutesOffset());
    }
}
