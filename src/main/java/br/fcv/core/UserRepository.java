package br.fcv.core;

import java.util.Collection;

import org.springframework.data.repository.CrudRepository;

import br.fcv.core.support.spring_data.FindExpectedRepositoryCustom;

public interface UserRepository extends CrudRepository<User, Long>, FindExpectedRepositoryCustom<User, Long> {

    public User findByUsername(String username);

    public long countByUsername(String username);

    public Iterable<User> findByRoleIn(Collection<Role> roles);

}
