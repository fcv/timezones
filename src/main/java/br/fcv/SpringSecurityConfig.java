package br.fcv;

import static br.fcv.core.Role.SYS_ADMIN;
import static java.util.Collections.singleton;

import java.util.Collection;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import br.fcv.core.Role;
import br.fcv.core.User;
import br.fcv.core.UserService;

/**
 * <p>
 * See more about Spring Security's configuration at
 * http://docs.spring.io/spring-security/site/docs/4.1.3.RELEASE/reference/html/jc.html
 * and a Getting Started tutorial at https://spring.io/guides/gs/securing-web/
 * </p>
 */
@Configuration
@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // disables CSRF for now in order to avoid authentication problems
        // in swagger's ui
        http.csrf().disable();

        // configures HTTP Basic authentication
        // consider configuring HTTP Digest ..
        // one possible implementation might be based on
        // http://stackoverflow.com/questions/18214296/digest-auth-with-spring-security-using-javaconfig
        http.httpBasic();

        http.authorizeRequests()
            // grant access to JS, .ico files to any request
            .antMatchers("/**/*.js", "/**/*.ico").permitAll()
            .antMatchers("/signup").anonymous()
            .antMatchers("/users", "/swagger*").hasAnyRole(SYS_ADMIN.name())
            .anyRequest()
                .authenticated();

        http.formLogin()
            .loginPage("/login")
            .permitAll();

        http.logout()
            .permitAll();
    }

    @Bean
    public UserDetailsService newUserDetailService(UserService userService, PasswordEncoder passwordEncoder) {
        return (username) -> {

            User user;
            UserDetails userDetails;
            String password;
            Role role;

            user = userService.findByUsername(username);
            if (user == null) {
                throw new UsernameNotFoundException("no user found for username '" + username + "'");
            }

            password = user.getPassword();
            role = user.getRole();
            String roleName = role.getRoleName();
            Collection<GrantedAuthority> grantedAuthorities = singleton(new SimpleGrantedAuthority(roleName));
            userDetails = new org.springframework.security.core.userdetails.User(username, password, grantedAuthorities);

            return userDetails;
        };
    }

    @Bean
    public PasswordEncoder newPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
