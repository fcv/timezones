package br.fcv;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import br.fcv.core.support.spring_data.FindExpectedRepositoryImpl;

@Configuration
@EnableJpaRepositories(repositoryBaseClass = FindExpectedRepositoryImpl.class)
public class SpringDataJpaConfig {

}
