package br.fcv;

import java.util.List;

import javax.inject.Inject;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import br.fcv.core.UserService;
import br.fcv.web.support.auth.LoggerUserArgumentResolver;

import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;

@Configuration
@EnableGlobalMethodSecurity(jsr250Enabled = true, prePostEnabled = true, securedEnabled = true)
public class SpringMvcConfig extends WebMvcConfigurerAdapter {

    @Inject
    private LoggerUserArgumentResolver loggerUserArgumentResolver;

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/home").setViewName("home");
        registry.addViewController("/").setViewName("home");
        registry.addViewController("/users").setViewName("users");
        registry.addViewController("/timezones").setViewName("timezones");
        registry.addViewController("/login").setViewName("login");
    }

    @Override
    public void addArgumentResolvers(
            List<HandlerMethodArgumentResolver> argumentResolvers) {

        argumentResolvers.add(loggerUserArgumentResolver);
    }

    @Bean
    public LoggerUserArgumentResolver newLoggedUserArgumentResolver(UserService userService) {
        return new LoggerUserArgumentResolver(userService);
    }

    @Bean
    public Module newHibernate5Module() {
        // it seems Jackson's Hibernate module is not automatically registered
        // registering it to avoid issues with lazy loaded properties like, for example an exception with message:
        // "Could not write content: No serializer found for class org.hibernate.proxy.pojo.javassist.JavassistLazyInitializer and no properties discovered to create BeanSerializer (to avoid exception, disable SerializationFeature.FAIL_ON_EMPTY_BEANS) (through reference chain: java.util.ArrayList[0]->br.fcv.core.TimeZone[\"createdBy\"]->br.fcv.core.User_$$_jvst9bf_0[\"handler\"]); nested exception is com.fasterxml.jackson.databind.JsonMappingException: No serializer found for class org.hibernate.proxy.pojo.javassist.JavassistLazyInitializer and no properties discovered to create BeanSerializer (to avoid exception, disable SerializationFeature.FAIL_ON_EMPTY_BEANS) (through reference chain: java.util.ArrayList[0]->br.fcv.core.TimeZone[\"createdBy\"]->br.fcv.core.User_$$_jvst9bf_0[\"handler\"])"
        // 
        // spring boot supports registering additional modules by exposing them as @Bean
        // see https://spring.io/blog/2014/12/02/latest-jackson-integration-improvements-in-spring#with-spring-boot
        // see more about configuring Jackson Object Mapper through propertie file at
        // http://docs.spring.io/spring-boot/docs/1.4.1.RELEASE/reference/html/howto-spring-mvc.html#howto-customize-the-jackson-objectmapper
        Module module = new Hibernate5Module();
        return module;
    }
}
