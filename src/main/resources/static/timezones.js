$(function($) {

	var $mainPanel = $('.container'),
		$modal = $('#timezone-modal', $mainPanel),
		$timezonesTable = $('.timezones-result-table', $mainPanel);

	$.fn.serializeObject = function() {
		// see http://stackoverflow.com/questions/1184624/convert-form-data-to-javascript-object-with-jquery/1186309#1186309
		var o = {};
		var a = this.serializeArray();
		$.each(a, function() {
			if (o[this.name] !== undefined) {
				if (!o[this.name].push) {
					o[this.name] = [ o[this.name] ];
				}
				o[this.name].push(this.value || '');
			} else {
				o[this.name] = this.value || '';
			}
		});
		return o;
	};

	function resetForm() {
		var $form = $('form', $modal);

		$form.each(function() {
			this.reset();
		});
	};

	function populateForm(zone) {
		var $form = $('form', $modal);

		$('[name=id]', $form).val(zone.id);
		$('[name=name]', $form).val(zone.name);
		$('[name=city]', $form).val(zone.city);
		$('[name=minutesOffset]', $form).val(zone.minutesOffset);
	};

	function save(evt) {

		evt.preventDefault();
		var $form = $('form', $modal),
			url = $form.attr('action');

		var id = $('[name=id]', $form).val(),
			method;

		if (id) {
			url = url + id;
			method = 'PUT';
		} else {
			method = 'POST';
		}

		var data = $form.serializeObject();
		return $.ajax({
			method: method,
			url: url,
			data: JSON.stringify(data),
			contentType: 'application/json',
			dataType: 'json',
			success: function() {
				// by now reload all zones again
				// consider only appending / updating specific row
				loadAll();
				$modal.modal('hide');
			}
		});
	};

	function removeTimezone(evt) {

		evt.preventDefault();

		var promise = null;
		if (confirm('Are you sure?')) {

			var $trigger = $(this),
				endpointUrl = $trigger.data('endpoint-url');

			promise = $.ajax({
				method: 'DELETE',
				url: endpointUrl,
				dataType: 'json',
			});

			promise.done(function() {
				$trigger.closest('tr').remove();
			});
		}
		return promise;
	};

	function openEditView(evt) {

		evt.preventDefault();

		var $trigger = $(this),
			endpointUrl = $trigger.data('endpoint-url');

		$.ajax({
			method: 'GET',
			url: endpointUrl,
			dataType: 'json'
		}).done(function(zone) {

			populateForm(zone);
			$modal.modal('show');
		})
	};

	function renderTimezones(zones) {
		var $tbody = $('tbody', $timezonesTable);

		var template = $('#timezone-row-tmpl').html();
		Mustache.parse(template);
		var rendered = Mustache.render(template, zones);
		$tbody
			.empty()
			.append(rendered);
	};

	function loadAll() {

		var endpointUrl = $timezonesTable.data('endpoint-url');
		var promise = $.ajax({
			method: 'GET',
			url: endpointUrl,
			dataType: 'json'
		});

		promise.done(renderTimezones);
		return promise;
	};

	function search(evt) {
		evt.preventDefault();

		var $form = $(this),
			endpointUrl = $form.attr('action');

		var promise = $.ajax({
			method: 'GET',
			url: endpointUrl,
			dataType: 'json',
			data: $form.serialize()
		});

		return promise.done(renderTimezones);
	};

	function init() {

		$modal.on('hidden.bs.modal', resetForm);
		$('.save-trigger', $modal).click(save);

		$timezonesTable
			.on('click', '.remove-trigger', removeTimezone)
			.on('click', '.open-edit-view-trigger', openEditView);

		$('#search-form', $mainPanel).submit(search);

		loadAll();
	};

	init();
});