$(function($) {

	var $mainPanel = $('.container'),
		$modal = $('#user-modal', $mainPanel),
		$usersTable = $('.users-result-table', $mainPanel);

	$.fn.serializeObject = function() {
		// see http://stackoverflow.com/questions/1184624/convert-form-data-to-javascript-object-with-jquery/1186309#1186309
		var o = {};
		var a = this.serializeArray();
		$.each(a, function() {
			if (o[this.name] !== undefined) {
				if (!o[this.name].push) {
					o[this.name] = [ o[this.name] ];
				}
				o[this.name].push(this.value || '');
			} else {
				o[this.name] = this.value || '';
			}
		});
		return o;
	};

	function resetForm() {
		var $form = $('form', $modal);

		$form.each(function() {
			this.reset();
		});
	};

	function populateForm(user) {
		var $form = $('form', $modal);

		$('[name=id]', $form).val(user.id);
		$('[name=username]', $form).val(user.username);
		$('[name=password]', $form).val(user.password);
		$('[name=role]', $form).val(user.role);
	};

	function save(evt) {

		evt.preventDefault();
		var $form = $('form', $modal),
			url = $form.attr('action');

		var id = $('[name=id]', $form).val(),
			method;

		if (id) {
			url = url + id;
			method = 'PUT';
		} else {
			method = 'POST';
		}

		var data = $form.serializeObject();
		return $.ajax({
			method: method,
			url: url,
			data: JSON.stringify(data),
			contentType: 'application/json',
			dataType: 'json',
			success: function() {
				// by now reload all users again
				// consider only appending / updating specific row
				loadAll();
				$modal.modal('hide');
			}
		});
	};

	function removeUser(evt) {

		evt.preventDefault();

		var promise = null;
		if (confirm('Are you sure?')) {

			var $trigger = $(this),
				endpointUrl = $trigger.data('endpoint-url');

			promise = $.ajax({
				method: 'DELETE',
				url: endpointUrl,
				dataType: 'json',
			});

			promise.done(function() {
				$trigger.closest('tr').remove();
			});
		}
		return promise;
	};

	function openEditView(evt) {

		evt.preventDefault();

		var $trigger = $(this),
			endpointUrl = $trigger.data('endpoint-url');

		$.ajax({
			method: 'GET',
			url: endpointUrl,
			dataType: 'json'
		}).done(function(user) {

			populateForm(user);
			$modal.modal('show');
		})
	};

	function renderUsers(users) {
		var $tbody = $('tbody', $usersTable);

		var template = $('#user-row-tmpl').html();
		Mustache.parse(template);
		var rendered = Mustache.render(template, users);
		$tbody
			.empty()
			.append(rendered);
	};

	function loadAll() {

		var endpointUrl = $usersTable.data('endpoint-url');
		var promise = $.ajax({
			method: 'GET',
			url: endpointUrl,
			dataType: 'json'
		});

		promise.done(renderUsers);
		return promise;
	};

	function init() {

		$modal.on('hidden.bs.modal', resetForm);
		$('.save-trigger', $modal).click(save);

		$usersTable
			.on('click', '.remove-trigger', removeUser)
			.on('click', '.open-edit-view-trigger', openEditView);

		loadAll();
	};

	init();
});