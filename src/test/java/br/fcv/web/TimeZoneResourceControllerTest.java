package br.fcv.web;

import static java.lang.Math.random;
import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.isA;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.util.Collection;
import java.util.function.Function;

import javax.persistence.EntityNotFoundException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import br.fcv.core.TimeZone;
import br.fcv.core.TimeZoneService;
import br.fcv.core.User;

import com.google.common.collect.ImmutableList;

/**
 * See more about testing Spring MVC along with Spring Security at http://docs.spring.io/spring-security/site/docs/4.1.x/reference/htmlsingle/#test-mockmvc
 * @author veronez
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@WithMockUser(username = "admin", password = "admin")
public class TimeZoneResourceControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private TimeZoneService timeZoneService;

    @Autowired
    private HttpMessageConverter<Object> mappingJackson2HttpMessageConverter;

    @Before
    public void setUp() throws Exception {
        ImmutableList<TimeZone> zones = ImmutableList.<TimeZone> builder()
                .add(new TimeZone(1L, "tz1", "city1", 60))
                .add(new TimeZone(2L, "tz2", "city2", -120))
                .build();
        when(timeZoneService.list(any(User.class))).thenReturn(zones);

        when(timeZoneService.update(any(TimeZone.class), any(User.class))).then(returnsFirstArg());

        when(timeZoneService.create(any(TimeZone.class))).then(returnsFirstArgTransformed(zone -> {
            zone.setId((long) random());
            return zone;
        }));

        when(timeZoneService.get(any(Long.class), any(User.class))).then(returnsFind(zones));
        when(timeZoneService.remove(any(Long.class), any(User.class))).then(returnsFind(zones));
    }

    @Test
    public void testGetAll() throws Exception {

        mvc.perform(get("/api/rest/v1/timezones"))
            .andExpect(status().isOk())
            .andExpect(content().contentTypeCompatibleWith(APPLICATION_JSON))
            .andExpect(jsonPath("$", hasSize(2)))
            .andExpect(jsonPath("$[*].id", equalTo(asList(1, 2))))
            .andExpect(jsonPath("$[*].city", equalTo(asList("city1", "city2"))))
            .andExpect(jsonPath("$[*].name", equalTo(asList("tz1", "tz2"))))
            .andExpect(jsonPath("$[*].minutesOffset", equalTo(asList(60, -120))))
            .andExpect(jsonPath("$[*].localTime.instant", everyItem(isA(Long.class))))
            .andExpect(jsonPath("$[*].localTime.formattedTime", everyItem(isA(String.class))))
            .andExpect(jsonPath("$[*].localTime.isoFormattedTime", everyItem(isA(String.class))))
            ;
    }

    @Test
    public void testGetOne() throws Exception {

        mvc.perform(get("/api/rest/v1/timezones/{id}", 1))
            .andExpect(status().isOk())
            .andExpect(content().contentTypeCompatibleWith(APPLICATION_JSON))
            .andExpect(jsonPath("$.id", equalTo(1)))
            .andExpect(jsonPath("$.city", equalTo("city1")))
            .andExpect(jsonPath("$.name", equalTo("tz1")))
            .andExpect(jsonPath("$.minutesOffset", equalTo(60)))
            .andExpect(jsonPath("$.localTime.instant", isA(Long.class)))
            .andExpect(jsonPath("$.localTime.formattedTime", isA(String.class)))
            .andExpect(jsonPath("$.localTime.isoFormattedTime", isA(String.class)))
            ;

        mvc.perform(get("/api/rest/v1/timezones/{id}", 671))
            .andExpect(status().isNotFound())
            ;
    }

    @Test
    public void testDeleteOne() throws Exception {

        mvc.perform(delete("/api/rest/v1/timezones/{id}", 1).with(csrf()))
            .andExpect(status().isNoContent())
            ;

        mvc.perform(delete("/api/rest/v1/timezones/{id}", 671).with(csrf()))
            .andExpect(status().isNotFound())
            ;
    }

    @Test
    public void testPutOne() throws Exception {

        String zoneJson = json(new TimeZone("new-tz1", "new-city1", 240));
        mvc.perform(put("/api/rest/v1/timezones/{id}", 1).with(csrf()).contentType(APPLICATION_JSON_UTF8).content(zoneJson))
            .andExpect(status().isOk())
            .andExpect(content().contentTypeCompatibleWith(APPLICATION_JSON))
            .andExpect(jsonPath("$.id", equalTo(1)))
            .andExpect(jsonPath("$.city", equalTo("new-city1")))
            .andExpect(jsonPath("$.name", equalTo("new-tz1")))
            .andExpect(jsonPath("$.minutesOffset", equalTo(240)))
            ;

        mvc.perform(delete("/api/rest/v1/timezones/{id}", 671).with(csrf()))
            .andExpect(status().isNotFound())
            ;
    }

    @Test
    public void testPostOne() throws Exception {

        String zoneJson = json(new TimeZone("new-tz", "new-city", 300));
        mvc.perform(post("/api/rest/v1/timezones").with(csrf()).contentType(APPLICATION_JSON_UTF8).content(zoneJson))
            .andExpect(status().isCreated())
            .andExpect(content().contentTypeCompatibleWith(APPLICATION_JSON))
            .andExpect(jsonPath("$.id", isA(Number.class)))
            .andExpect(jsonPath("$.city", equalTo("new-city")))
            .andExpect(jsonPath("$.name", equalTo("new-tz")))
            .andExpect(jsonPath("$.minutesOffset", equalTo(300)))
            ;
    }

    private static Answer<TimeZone> returnsFirstArgTransformed(Function<TimeZone, TimeZone> transformation) {
        return new Answer<TimeZone>() {

            @Override
            public TimeZone answer(InvocationOnMock invocation)
                    throws Throwable {
                TimeZone timeZone = (TimeZone) invocation.getArguments()[0];
                return transformation.apply(timeZone);
            }
        };
    }

    private static Answer<TimeZone> returnsFind(Collection<TimeZone> zones) {
        return new Answer<TimeZone>() {

            @Override
            public TimeZone answer(InvocationOnMock invocation)
                    throws Throwable {
                Long id = (Long) invocation.getArguments()[0];
                return zones.stream()
                    .filter(z -> id.equals(z.getId()))
                    .findFirst().orElseThrow(EntityNotFoundException::new);
            }
        };
    }

    protected String json(Object o) throws IOException {
        // based on https://spring.io/guides/tutorials/bookmarks/
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(
                o, APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }
}
